package test_boa_api;

public class MainTest {
	public static void main(String[] args) {
		ArchiveReader reader = new ArchiveReader("in.txt");
		BOARequestManager request = new BOARequestManager();
		request.login();
		request.makeQueryWith(reader.readFile());
		request.verifyJobStatus();
	}
}
