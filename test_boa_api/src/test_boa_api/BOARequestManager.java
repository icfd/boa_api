package test_boa_api;

import edu.iastate.cs.boa.BoaClient;
import edu.iastate.cs.boa.BoaException;
import edu.iastate.cs.boa.ExecutionStatus;
import edu.iastate.cs.boa.LoginException;
import edu.iastate.cs.boa.NotLoggedInException;
import edu.iastate.cs.boa.InputHandle;
import edu.iastate.cs.boa.JobHandle;

public class BOARequestManager {
	private static final String login = "icfdamascena";
	private static final String password = "imightbecrazy4u";
	private BoaClient _client;
	private InputHandle _inputHandle;
	private JobHandle _job;
	
	public void login(){
		try {
			BoaClient client = new BoaClient();
			client.login(login, password);
			
			this._client = client;
			System.out.println("Login succeed!");
		}catch(LoginException e){
			System.out.println("Couldnt login!");
		}
		
	}
	
	public void makeQueryWith(String query){
		try {
			this.inputHandlerRequest();
			this._job = this._client.query(query, this._inputHandle);
			System.out.println("Request make");
			System.out.println(this._job);
			
		} catch (BoaException e) {
			System.out.println("message " + e.getMessage());
			System.out.println("stack trace below");
			e.getStackTrace();
		}
	}
	
	public void inputHandlerRequest(){
		try {
		
			for ( final InputHandle input : this._client.getDatasets()){
				if (input.getId() == 13){
					this._inputHandle = input;
					break;
				}
			}
			
		}catch(BoaException e){
			System.out.println("Couldnt get the input handle database!");
		}
	}
	
	public void verifyJobStatus() {
		System.out.println("Verifying last job`s status");
		
		outerloop:
		while (true){
			
			try {
				this._job.refresh();
			} catch (NotLoggedInException e1) {
				e1.printStackTrace();
			} catch (BoaException e1) {
				e1.printStackTrace();
			}
			ExecutionStatus exeStatus = this._job.getExecutionStatus();
			
			switch( exeStatus) {
			
			case ERROR:
				System.out.println("Execution Error!");
				break outerloop;
			case FINISHED:
				//do some work to show output
				System.out.println("Terminated!");
				this.getOutput();
				break outerloop;
			case WAITING:
				System.out.println("Processing!");
				break;	
			}
			
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void getOutput(){
		try {
			System.out.println(this._job.getOutput());
		} catch (BoaException e) {
			e.printStackTrace();
		}
	}
}
