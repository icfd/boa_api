package test_boa_api;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ArchiveReader {
	private String _inputPath;
	
	ArchiveReader(String inputPath){
		this._inputPath = inputPath;
	}
	
	public String readFile(){
		try {
			byte [] encoded = Files.readAllBytes(Paths.get(this._inputPath));
			return new String(encoded, Charset.defaultCharset());
			
		} catch ( IOException e){
			System.out.println("The program couldnt find the archive");
			return null;
		}
		
	}
}
